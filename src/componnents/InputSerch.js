
import React, { useState } from 'react'

import { useSerch } from "../Store/Serch"

function InputSerch() {
   
    const { dispatch, SerchInfo } = useSerch()
    const handleSerch = (val) => {
        dispatch(SerchInfo({
            name: val,
        }))
    }
    return (
        <>
            <div class="p-8">
                <div class="bg-white flex items-center rounded-lg shadow-xl">
                    <input class="rounded-lg w-full py-4 px-6 text-gray-700 leading-tight focus:outline-none" id="search" type="text" placeholder="جستجو"
                        
                        onChange={(e) => handleSerch(e.target.value)}
                    />
                    <div class="p-4">
                        <button class="bg-green-500 text-white rounded-lg px-8 text-wei hover:green-400 focus:outline-none w-12 h-12 flex items-center justify-center"
                            onClick={handleSerch}
                        >
                            جستجو
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default InputSerch
