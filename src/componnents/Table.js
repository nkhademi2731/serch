import React from 'react'
import RowTable from "./RowTable"
import { useSerch } from "../Store/Serch"

function Table() {
    const { searchResualt } = useSerch()
    return (
        <>

         

            <div className="flex flex-col p-8 ">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table className="min-w-full divide-y divide-gray-200 bg-gray-100">
                                <thead className="">
                                    <tr>
                                     
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                        موضوع    
                                       </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                           عنوان 
                                        </th>

                                        <th scope="col" className="relative px-6 py-3">
                                            <span className="sr-only">Edit</span>
                                        </th>
                                    </tr>
                                </thead>
                                {searchResualt && searchResualt.length > 0 && searchResualt.map((item, index) => {
                                    return <RowTable key={item.id} item={item} index={index} />
                                })}
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </>
    )
}

export default Table
