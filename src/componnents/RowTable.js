import React from 'react'

function RowTable({ item, index }) {
    return (
        <>
            {/* <tbody className="bg-red-100">

                <tr>
                    <td>{item.name}</td>
                    <td>{item.description}</td>
                    <td>{index++}</td>
                </tr>


            </tbody> */}

            <tbody className="bg-white divide-y divide-gray-200">
           
                <tr >
                    <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                            <div className="flex-shrink-0 h-10 w-10">
                           # {index+1}
                            </div>
                            <div className="ml-4">
                                <div className="text-sm font-medium text-gray-900">{item.name}</div>
                                <div className="text-sm text-gray-500">{item.description}</div>
                            </div>
                        </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                        <div className="text-sm text-gray-900">{item.name}</div>
                        <div className="text-sm text-gray-500">4</div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                        <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                            موجود
                      </span>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500"></td>
                   
                </tr>

            </tbody>

        </>
    )
}

export default RowTable
