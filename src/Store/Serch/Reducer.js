import React from "react"
import { SEARCH_INFO } from "./Actiones"

export const AllState = {
    data: [{
        id: 1,
        name: "react",
        description: "reactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreact"
    },
    {
        id: 2,
        name: "redux",
        description: "reduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxredux",
    },
    {
        id: 3,
        name: "javascript",
        description: "javascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascript",
    },
    {
        id: 4,
        name: "jquery",
        description: "jqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjquery",
    },
    {
        id: 5,
        name: "لورم ایپسوم",
        description: "از شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد"
        
    }],
    searchResualt: [{
        id: 1,
        name: "react",
        description: "reactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreactreact"
    },
    {
        id: 2,
        name: "redux",
        description: "reduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxreduxredux",
    },
    {
        id: 3,
        name: "javascript",
        description: "javascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascriptjavascript",
    },
    {
        id: 4,
        name: "javascript",
        description: "jqueryjqueryjqueryjavascriptjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjqueryjquery",
    },
    {
        id: 5,
        name: "لورم ایپسوم",
        description: "از شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد"  
    }]
    


}


export default (state = AllState, action) => {
    switch (action.type) {
        case SEARCH_INFO:
            return {
                data: state.data,
                searchResualt: state.data.filter(item => item.description.includes(action.payload.name))
            }

        default: return state;
    }
}