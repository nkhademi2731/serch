import React from "react"
import { useSelector, useDispatch } from "react-redux"


export const SEARCH_INFO = "SerchInfo"

export const SerchInfo = (val) => (dispatch) => {
    dispatch({
        type: SEARCH_INFO,
        payload: val,
    })
}

export const useSerch = () => {
    const Serch = useSelector((state) => state.Serch)
    const dispatch = useDispatch()
    return {
        ...Serch,
        dispatch,
        SerchInfo
    }
}