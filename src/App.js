import './App.css';
import { Provider } from 'react-redux'

// add componnents
import store from "./Store"
import { InputSerch, Table } from "./componnents"


function App() {


  return (
    <>
      <Provider store={store} >
        <div class="container mx-auto">

          <InputSerch />
          <Table />
        
        </div>
      </Provider>
    </>
  );
}

export default App;
